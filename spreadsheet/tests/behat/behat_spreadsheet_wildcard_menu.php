<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Behat spreadsheet-generation steps definitions moodle side 
 *
 * @package   local_spreadsheet
 * @category  test
 * @author    Jesus G. Vega Alejandro
 */

// NOTE: no MOODLE_INTERNAL used, this file may be required by behat before including /config.php.
require_once(__DIR__ . '/../../../lib/behat/behat_base.php');


/**
 * Contains functions used by behat to test functionality.
 *
 * @package   local_spreadsheet
 * @category  test
 * @author    Jesus G. Vega Alejandro
 */
class behat_spreadsheet extends behat_base {
    
 //To manage during testing phase   
    

}
    