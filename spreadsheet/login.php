<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.




require_once('../../config.php');
require("$CFG->dirroot/local/spreadsheet/lib.php");
require("$CFG->dirroot/local/spreadsheet/api_validate.php");


/**
 * Google Services Authentication. This file is a simple login entry point for OAuth identity providers to corroborate authentication and provide request exchanges.
 * @package local_spreadsheet
 * @author Jesus G. Vega Aleajandro
 *
 */



global $DB, $USER, $COURSE;

require_login();

$json = file_get_contents('credentials.json');
if(!is_null($json) && is_string($json) && $json!='false'){
    $client = new Google_Client_mod();
    
    //Format project Client with Google Web Application Set Parameters. Confiuration Parameters established at https://console.developers.google.com
    $client->format_client($json, ['https://www.googleapis.com/auth/drive', 'https://www.googleapis.com/auth/spreadsheets'], "PHP_Spreadsheets_Generation");
   
    //Manager for client request. Retrieve HTTPS code for login credential information
    if (isset($_GET['oauth2code'])) {
        $client->authenticate($_GET['oauth2code']);
        $token = $client->getAccessToken();
        $client->setAccessToken($token);
        $client->setConfig('CURLOPT_CONNECTTIMEOUT', 30);
        $client->setConfig('CURLOPT_TIMEOUT', 30);
    
        if(!$client->isAccessTokenExpired()){ 
        
             //Generate Service and the Moodle/Spreadsheet Management Service to handle both local DB and external queries
             header("Connection: close");
             $service =  new Google_Spreadsheet($client);
             $drive_service = new Google_Service_Drive($client);
             $service_local_management = new Moodle_Spreadsheet_Management($service, $drive_service);
             
             
             //Necessary Quiz parameters stored as session/cookie values.It was not feasible to pass by URL. Critical parameters passed by $_SESSION (secured).
              $spreadsheet_values = $_SESSION['spreadsheets_values'];
              $cmid = $spreadsheet_values["spreadsheet_cmid"];
              $attemptid = $spreadsheet_values["spreadsheet_attemptid"];
              $attempt = $spreadsheet_values["spreadsheet_attempts"]; 
              $page = $spreadsheet_values["spreadsheet_page"];
              $quizID = $spreadsheet_values['spreadsheet_quizId'];
              $quiz_name = $spreadsheet_values['spreadsheet_quiz_name'];
              $student_id  = $spreadsheet_values['spreadsheet_student_id'];
              $student_name = $spreadsheet_values['spreadsheet_student_name'];
              $question_name =  str_replace(' ', '', $spreadsheet_values['spreadsheet_question_name']);
              $data_range= $spreadsheet_values["spreadsheet_data_range"] ?? 0;
              $image_range= $spreadsheet_values["spreadsheet_image_range"] ?? 0;
              $spreadsheet_id = $spreadsheet_values["spreadsheet_gs_id"] ?? 0;
              $sheetid = $spreadsheet_values["spreadsheet_sheet_id"] ?? 0; 
              
             //*************************************************************************************************************************************//
              try{
                if(!is_null($spreadsheet_id) && is_string($spreadsheet_id) && strlen($spreadsheet_id)>0){
                    try{
                        $service_local_management->manage_sheet_update($spreadsheet_id, $sheetid, $question_name, $attempt, $data_range,null, $image_range, $student_id, $quizID, true, 1, $student_name, $quiz_name);
                    }catch(\Exception $e){ 
                      if($e->getCode()=='404'){
                          \core\notification::error(get_string('Sheet_Not_Found', 'local_spreadsheet'));
                      }
                    }
                }else{
                    //If there is no local reference to a spreadsheet file, then a sheet is generated and stored in the database. 
                    try{
                        $sheet_record= $DB->get_record('spreadsheets_validation' , ['user_id' => $USER->id, 'quiz_id' => $quizID]);
                    }catch (dml_missing_record_exception $e){$sheet_record= false;}
                    
                    if(!$sheet_record){
                        $spreadsheet= $service_local_management->manage_sheet_create($quizID, $question_name, $quiz_name);
                        $service_local_management->manage_override_sheet_values($spreadsheet[0], $spreadsheet[1], 
                            $question_name, $attempt,$data_range, null, $image_range, $student_id, false, false, 0, $student_name, $quizID);
                    }else{
                        $spreadsheetId = $sheet_record->sheet_id;
                        $sheet_external = $sheet_record->is_external;
                        $service_local_management->manage_sheet_update($spreadsheetId, null, $question_name, $attempt, 
                            $data_range, null, $image_range, $student_id, $quizID, false, $sheet_external, $student_name, $quiz_name); 
                    }
                }
              }catch(\Exception $e){ 
                  if($e->getCode()=='403' || $e->getCode()=='429'){
                      \core\notification::error(get_string('REQUEST_ERROR', 'local_spreadsheet'));
                  }
              }
              
             
             redirect(new moodle_url("/mod/quiz/review.php?attempt=" . $attemptid. "&cmid=" . 
             $cmid . '&showall=0' . '&page='. $page));    
    }else{
        /** 
         * TODO Verify aspects of incorporating a Refresh token request for extended session scheme IF NECESSARY
         * Token Expires in 30 mins for Google Services at that point Authentication is asked again
         */
        
        }
    }else{
        $sess = sesskey();
        $client->setState("/local/spreadsheet/login.php?sesskey=$sess");
        $client->setRedirectUri('https://' . $_SERVER['HTTP_HOST'] . '/admin/oauth2callback.php');
        $authUrl = $client->createAuthUrl();
        header("Location:" . filter_var($authUrl, FILTER_SANITIZE_URL));}
}else{
    
    if(has_capability('moodle/site:config', get_context_instance(CONTEXT_COURSE, $COURSE->id)) && is_siteadmin($USER->id)){
        \core\notification::info(get_string('MISSING_JSON_ADMIN', 'local_spreadsheet'));
        redirect(new moodle_url("/local/spreadsheet/configure.php"));
    }else{
        \core\notification::error(get_string('MISSING_JSON_TEACHER',  'local_spreadsheet'));
        $spreadsheet_values = $_SESSION['spreadsheets_values'];
        redirect(new moodle_url("/mod/quiz/review.php?attempt=" . $spreadsheet_values["spreadsheet_attemptid"] . "&cmid=" .
            $spreadsheet_values["spreadsheet_cmid"] . '&showall=0' . '&page='. $spreadsheet_values["spreadsheet_page"]));
    }
    throw new coding_exception('JSON Project Credentials Authentication file is null or cannot be decoded');
}


