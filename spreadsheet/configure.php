<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.




require_once('../../config.php');
require_once($CFG->dirroot . '/local/spreadsheet/classes/upload_form.php');

global $PAGE, $CGF, $USER;

require_login();

$PAGE->set_url(new moodle_url("$CFG->dirroot/local/spreadsheet/configure.php"));
$PAGE->set_context(\context_system::instance());
$PAGE->set_title("Upload Configuration File: Spreadsheet Plugin");
$PAGE->set_heading("Upload Configuration File: Spreadsheet Plugin");

$main_block = new upload_form;


echo $OUTPUT->header();


if(has_capability('moodle/site:config', context_course::instance($COURSE->id)) && is_siteadmin($USER->id)){
    echo $main_block->def();
}else{
    \core\notification::error(get_string('UPLOAD_FORM_PERMISSION_DENIED', 'local_spreadsheet'));
}

echo $OUTPUT->footer();


