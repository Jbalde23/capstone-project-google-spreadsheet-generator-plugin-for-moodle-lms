<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This page prints a review of a particular quiz attempt
 *
 * It is used either by the student whose attempts this is, after the attempt,
 * or by a teacher reviewing another's attempt during or afterwards.
 *
 * @package   mod_quiz
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once(__DIR__ . '/../../config.php');
require_once($CFG->dirroot . '/mod/quiz/locallib.php');
require_once($CFG->dirroot . '/mod/quiz/report/reportlib.php');

global $USER;

$attemptid = required_param('attempt', PARAM_INT);
$page      = optional_param('page', 0, PARAM_INT);
$showall   = optional_param('showall', null, PARAM_BOOL);
$cmid      = optional_param('cmid', null, PARAM_INT);

$url = new moodle_url('/mod/quiz/review.php', array('attempt'=>$attemptid));
if ($page !== 0) {
    $url->param('page', $page);
} else if ($showall) {
    $url->param('showall', $showall);
}
$PAGE->set_url($url);

$attemptobj = quiz_create_attempt_handling_errors($attemptid, $cmid);
$page = $attemptobj->force_page_number_into_range($page);

// Now we can validate the params better, re-genrate the page URL.
if ($showall === null) {
    $showall = $page == 0 && $attemptobj->get_default_show_all('review');
}
$PAGE->set_url($attemptobj->review_url(null, $page, $showall));

// Check login.
require_login($attemptobj->get_course(), false, $attemptobj->get_cm());
$attemptobj->check_review_capability();

// Create an object to manage all the other (non-roles) access rules.
$accessmanager = $attemptobj->get_access_manager(time());
$accessmanager->setup_attempt_page($PAGE);

$options = $attemptobj->get_display_options(true);

// Check permissions - warning there is similar code in reviewquestion.php and
// quiz_attempt::check_file_access. If you change on, change them all.
if ($attemptobj->is_own_attempt()) {
    if (!$attemptobj->is_finished()) {
        redirect($attemptobj->attempt_url(null, $page));
        
    } else if (!$options->attempt) {
        $accessmanager->back_to_view_page($PAGE->get_renderer('mod_quiz'),
            $attemptobj->cannot_review_message());
    }
    
} else if (!$attemptobj->is_review_allowed()) {
    throw new moodle_quiz_exception($attemptobj->get_quizobj(), 'noreviewattempt');
}

// Load the questions and states needed by this page.
if ($showall) {
    $questionids = $attemptobj->get_slots();
} else {
    $questionids = $attemptobj->get_slots($page);
}

// Save the flag states, if they are being changed.
if ($options->flags == question_display_options::EDITABLE && optional_param('savingflags', false,
    PARAM_BOOL)) {
        require_sesskey();
        $attemptobj->save_question_flags();
        redirect($attemptobj->review_url(null, $page, $showall));
    }
    
    // Work out appropriate title and whether blocks should be shown.
    if ($attemptobj->is_own_preview()) {
        navigation_node::override_active_url($attemptobj->start_attempt_url());
        
    } else {
        if (empty($attemptobj->get_quiz()->showblocks) && !$attemptobj->is_preview_user()) {
            $PAGE->blocks->show_only_fake_blocks();
        }
    }
    
    // Set up the page header.
    $headtags = $attemptobj->get_html_head_contributions($page, $showall);
    $PAGE->set_title($attemptobj->review_page_title($page, $showall));
    $PAGE->set_heading($attemptobj->get_course()->fullname);
    
    // Summary table start. ============================================================================
    
    // Work out some time-related things.
    $attempt = $attemptobj->get_attempt();
    $quiz = $attemptobj->get_quiz();
    $overtime = 0;
    
    if ($attempt->state == quiz_attempt::FINISHED) {
        if ($timetaken = ($attempt->timefinish - $attempt->timestart)) {
            if ($quiz->timelimit && $timetaken > ($quiz->timelimit + 60)) {
                $overtime = $timetaken - $quiz->timelimit;
                $overtime = format_time($overtime);
            }
            $timetaken = format_time($timetaken);
        } else {
            $timetaken = "-";
        }
    } else {
        $timetaken = get_string('unfinished', 'quiz');
    }
    
    // Prepare summary informat about the whole attempt.
    $summarydata = array();
    if (!$attemptobj->get_quiz()->showuserpicture && $attemptobj->get_userid() != $USER->id) {
        // If showuserpicture is true, the picture is shown elsewhere, so don't repeat it.
        $student = $DB->get_record('user', array('id' => $attemptobj->get_userid()));
        $userpicture = new user_picture($student);
        $userpicture->courseid = $attemptobj->get_courseid();
        $summarydata['user'] = array(
            'title'   => $userpicture,
            'content' => new action_link(new moodle_url('/user/view.php', array(
                'id' => $student->id, 'course' => $attemptobj->get_courseid())),
                fullname($student, true)),
        );
    }
    
    if ($attemptobj->has_capability('mod/quiz:viewreports')) {
        $attemptlist = $attemptobj->links_to_other_attempts($attemptobj->review_url(null, $page,
            $showall));
        if ($attemptlist) {
            $summarydata['attemptlist'] = array(
                'title'   => get_string('attempts', 'quiz'),
                'content' => $attemptlist,
            );
        }
    }
    
    
    // Timing information.
    $summarydata['startedon'] = array(
        'title'   => get_string('startedon', 'quiz'),
        'content' => userdate($attempt->timestart),
    );
    
    $summarydata['state'] = array(
        'title'   => get_string('attemptstate', 'quiz'),
        'content' => quiz_attempt::state_name($attempt->state),
    );
    
    if ($attempt->state == quiz_attempt::FINISHED) {
        $summarydata['completedon'] = array(
            'title'   => get_string('completedon', 'quiz'),
            'content' => userdate($attempt->timefinish),
        );
        $summarydata['timetaken'] = array(
            'title'   => get_string('timetaken', 'quiz'),
            'content' => $timetaken,
        );
    }
    
    if (!empty($overtime)) {
        $summarydata['overdue'] = array(
            'title'   => get_string('overdue', 'quiz'),
            'content' => $overtime,
        );
    }
    
    // Show marks (if the user is allowed to see marks at the moment).
    $grade = quiz_rescale_grade($attempt->sumgrades, $quiz, false);
    if ($options->marks >= question_display_options::MARK_AND_MAX && quiz_has_grades($quiz)) {
        
        if ($attempt->state != quiz_attempt::FINISHED) {
            // Cannot display grade.
            
        } else if (is_null($grade)) {
            $summarydata['grade'] = array(
                'title'   => get_string('grade', 'quiz'),
                'content' => quiz_format_grade($quiz, $grade),
            );
            
        } else {
            // Show raw marks only if they are different from the grade (like on the view page).
            if ($quiz->grade != $quiz->sumgrades) {
                $a = new stdClass();
                $a->grade = quiz_format_grade($quiz, $attempt->sumgrades);
                $a->maxgrade = quiz_format_grade($quiz, $quiz->sumgrades);
                $summarydata['marks'] = array(
                    'title'   => get_string('marks', 'quiz'),
                    'content' => get_string('outofshort', 'quiz', $a),
                );
            }
            
            // Now the scaled grade.
            $a = new stdClass();
            $a->grade = html_writer::tag('b', quiz_format_grade($quiz, $grade));
            $a->maxgrade = quiz_format_grade($quiz, $quiz->grade);
            if ($quiz->grade != 100) {
                $a->percent = html_writer::tag('b', format_float(
                    $attempt->sumgrades * 100 / $quiz->sumgrades, 0));
                $formattedgrade = get_string('outofpercent', 'quiz', $a);
            } else {
                $formattedgrade = get_string('outof', 'quiz', $a);
            }
            $summarydata['grade'] = array(
                'title'   => get_string('grade', 'quiz'),
                'content' => $formattedgrade,
            );
        }
    }
    
    // Any additional summary data from the behaviour.
    $summarydata = array_merge($summarydata, $attemptobj->get_additional_summary_data($options));
    
    // Feedback if there is any, and the user is allowed to see it now.
    $feedback = $attemptobj->get_overall_feedback($grade);
    if ($options->overallfeedback && $feedback) {
        $summarydata['feedback'] = array(
            'title'   => get_string('feedback', 'quiz'),
            'content' => $feedback,
        );
        
        
        
    }
    
    
    // Summary table end. ==============================================================================
    
    if ($showall) {
        $slots = $attemptobj->get_slots();
        $lastpage = true;
    } else {
        $slots = $attemptobj->get_slots($page);
        $lastpage = $attemptobj->is_last_page($page);
    }
    
    // Plugin Patch Code . ==============================================================================================
    if($attemptobj->has_capability('moodle/course:manageactivities', $USER->id) && ($attemptobj->get_question_type_name($slots[0])=='calculated'
        || $attemptobj->get_question_type_name($slots[0])=='calculatedmulti' || $attemptobj->get_question_type_name($slots[0])=='calculatedsimple')){
            
            if(!$showall){
                global $CFG, $DB;
                require_once("$CFG->dirroot/local/spreadsheet/classes/data_form_mod.php");
                
                if(!isset($student)){
                    $student = $DB->get_record('user', array('id' => $attemptobj->get_userid()));
                }
                
                $button_block = new block_contents();
                $attempts_id = $DB->get_field("question_attempt_steps", 'id', ['questionattemptid'=>$attemptobj->get_question_attempt($slots[0])->get_database_id(),
                    'state' => "todo"]);
                
                $student_name = fullname($student);
                $question_unique_id = $attemptobj->get_question_attempt($slots[0])->get_question_id();
                $validation = 0;
                
                //Prepare Question Values
                if(!isset($_SESSION['spreadsheets_values']) ||
                    (!isset($_SESSION['spreadsheets_values']['spreadsheet_question_id']) && !isset($_SESSION['spreadsheets_values']['spreadsheet_student_name']))){
                        $validation = 1;
                }else{
                    $vals = $_SESSION['spreadsheets_values'];
                    if(!(($vals['spreadsheet_question_id'] == $attemptobj->get_question_attempt($slots[0])->get_question_id()) &&
                        ($vals['spreadsheet_student_name'] == $student_name))){
                            $validation = 1;
                    }
                }
                if($validation==1){
                    $data_objects = $DB->get_records_select('question_attempt_step_data', "attemptstepid=$attempts_id");
                    $spreadsheet_data_variables= array();
                    $spreadsheet_data_values = array();
                    foreach($data_objects as $values){
                        array_push($spreadsheet_data_variables, $values->name);
                        array_push($spreadsheet_data_values, $values->value);
                    }
                    $spreadsheet_unit_values = $DB->get_fieldset_select('question_numerical_units', 'unit', "question=$question_unique_id");
                    
                    $_SESSION['spreadsheet_writting_values'] = [
                        'spreadsheet_data_variables' => $spreadsheet_data_variables,
                        'spreadsheet_data_values' => $spreadsheet_data_values,
                        'spreadsheet_unit_values'=> $spreadsheet_unit_values,
                    ];
                }
                
                $buttons = new block_form_sheets($attemptobj->get_quizid(), $attemptobj->get_quiz_name(), $question_unique_id,
                    $attemptobj->get_question_attempt($slots[0])->get_question()->name, $attemptobj->get_cmid(), $attempts_id, $attemptid, $attemptobj->get_userid(), $attemptobj->get_courseid(), $page, $student_name);
                $button_block->content = $buttons->def();
                
            }else{
                $button_block = new block_contents();
                $redirect_url = new moodle_url("/mod/quiz/review.php?attempt=" . $attemptid. "&cmid=" .
                    $attemptobj->get_cmid() . '&showall=0');
                $button_block->content = "<form>
        <label for='Generation'><h5>Generate Google Spreadsheet</h5> </label><br>
        <a href = $redirect_url>Enable Google Spreadsheets Generation by accessing the single question display page</a>
        </form>";
            }
            
    } else $button_block = new block_contents();
    // Plugin Patch Code . ==============================================================================================
    
    $output = $PAGE->get_renderer('mod_quiz');
    
    // Arrange for the navigation to be displayed.
    $navbc = $attemptobj->get_navigation_panel($output, 'quiz_review_nav_panel', $page, $showall);
    $regions = $PAGE->blocks->get_regions();
    $PAGE->blocks->add_fake_block($navbc, reset($regions));
    
    // Plugin Patch Code . ==============================================================================================
    $PAGE->blocks->add_fake_block($button_block, reset($regions));
    // Plugin Patch Code . ==============================================================================================
    
    echo $output->review_page($attemptobj, $slots, $page, $showall, $lastpage, $options, $summarydata);
    
    // Trigger an event for this review.
    $attemptobj->fire_attempt_reviewed_event();
    
