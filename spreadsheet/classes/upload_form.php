<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


defined('MOODLE_INTERNAL') || die();

global $CFG; 

class upload_form{
    
        //Defines a simple form to upload an acceptable JSON file 
        public function def(): ?string{
            
        $help_string = get_string('UPLOAD_FORM', 'local_spreadsheet');  
        $form_definition = "<form action='upload.php' method='post' enctype='multipart/form-data'>
        <p>$help_string</p>
        Select project configuration file to upload:
        <input type='file' name='fileToUpload' id='fileToUpload'>
        <input type='submit' value='Upload File' name='submit'>
        </form>";
        
        return $form_definition;
        
    }

    
    
    
}