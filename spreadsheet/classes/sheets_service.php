<?php
use Google\Exception;

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


defined('MOODLE_INTERNAL') || die();


require_once("$CFG->dirroot/local/spreadsheet/api/google/apiclient-services/src/Google/Service/Sheets/Spreadsheet.php");
/**
 * @name Moodle_Spreadsheet_Management
 * @author Jesus G. Vega Alejandro
 * @desc This class contains a variety of functions which allows integration between Moodle Question data and Google Spreadsheets
 */

class Moodle_Spreadsheet_Management{
    
    public $service;
    private $values;
    private $drive_service;
    
   
    public function __construct(Google_Spreadsheet $sheet_service, Google_Service_Drive $drive_services){
        $this->service = $sheet_service->spreadsheets;
        $this->values = $sheet_service->spreadsheets_values;
        $this->drive_service = $drive_services;
    }
    
    public function delete_sheet($spreadsheetId){
        $existing_sheet = Moodle_Spreadsheet_Management::$service->check_sheet($spreadsheetId);
        if(!is_null($existing_sheet)){
            Moodle_Spreadsheet_Management::$drive_service->files->delete($spreadsheetId);
        }
    }
    
    
    /**
     * @name manage_sheet_update
     * @author Jesus G. Vega Alejandro
     * @desc This method updates the local database with the url of the newly created spreadsheet in the driveand initiates writting protocols. 
     * @param string $spreadshee_id, $sheet_id, $question_name, $data_range, $student_id
     * @param int $attepms, $quizId
     * @param bool $is_external
     */
    public function manage_sheet_update($spreadshee_id, $sheet_id = null ,$question_name, $attempts, $data_range, $image_included, 
        $image_range, $student_id, $quizId, $is_external, $saved_external = null, $student_name = null, $quiz_name = null): ?string{
        global $DB, $USER;
        //Worst Case Total API Calls: 3 
        
        //The following is a necessary Query to verify that the spreadsheet to be updated exists within the google drive.
        $existing_sheet = $this->service->check_sheet($spreadshee_id); 
        
        if(is_null($existing_sheet)){
            $quizTitle = $quiz_name;
            remove_link($quizId, $USER->id);
            //2 API Call
            $spreadsheet_obj = $this->service->create_question_sheet($quizTitle, $question_name);
            $spreadsheet_id = $spreadsheet_obj[0];
            $sheet_id = $spreadsheet_obj[1];
            $this->manage_override_sheet_values($spreadsheet_id,$sheet_id, $question_name, $attempts,
                $data_range, $image_included,  $image_range, $student_id, null ,$is_external, $saved_external, $student_name, $quizId);
            
            //Generates a valid redirect link and stores it in the Database
            $sheet_url= 'docs.google.com/spreadsheets/d/' . $spreadsheet_id;
            insert_link($spreadsheet_id,$sheet_url,$quizId, $sheet_id, $is_external);
            return $spreadsheet_id;
        }elseif(!is_null($existing_sheet)){
            if($this->check_sheet($existing_sheet->getSheets(),$question_name, $sheet_id)){
                \core\notification::info(get_string('Duplicate_Shet_Title', 'local_spreadsheet'));
                return null;
            }else{
                //Update the sheet title before writting to sheet. The sheet to write is identified by the sheet name.
                if(!is_null($sheet_id)){
                    if($existing_sheet->getSheets()[0]->getProperties()->getTitle() != $question_name)
                    $this->service->update_question_sheet_title($existing_sheet->spreadsheetId, $question_name, $sheet_id);
                }else{
                    $sheet_id = $DB->get_field('spreadsheets_validation', 'single_sheet_id', ['user_id'=>$USER->id, 'quiz_id'=>$quizId]);
                    if($existing_sheet->getSheets()[0]->getProperties()->getTitle() != $question_name)
                        $this->service->update_question_sheet_title($existing_sheet->spreadsheetId, $question_name, $sheet_id);
                }
                
                //If the SpreadsheetID and the SheetID were from an External Spreadsheet created by the User. The references in the local databases are updated. 
                if($is_external){
                    remove_link($quizId, $USER->id);
                    $sheet_url= 'docs.google.com/spreadsheets/d/' . $spreadshee_id;
                    insert_link($spreadshee_id,$sheet_url,$quizId, $sheet_id, $is_external);
                }
                //Manage write call to generated sheet through the SpreadsheetID
                $this->manage_override_sheet_values($spreadshee_id, $sheet_id, $question_name, $attempts,
                    $data_range, $image_included,  $image_range, $student_id, null, $is_external, $saved_external, $student_name, $quizId);
            }
        }
       return $spreadshee_id;
    }
    
  
    
    
    /**
     * @name check_file
     * @author Jesus G. Vega Alejandro
     * @desc Helper function to verify the exstance of a file in drive through a Google Drive Request.
     * @param string $file_id
     * @return Google_Service_Drive_DriveFile or Null
     */
    private function check_file($file_id): ?Google_Service_Drive_DriveFile{
        try{ $file = $this->drive_service->files->get($file_id);
        }catch(\Exception $e){ if($e->getCode()=='404') $file = null;}
        return $file;
    }
    
    /**
     * @name check_sheet
     * @author Jesus G. Vega Alejandro
     * @desc Helper function to verify the exstance of a spreadsheet in drive through a Google Sheet Request.
     * @param array $sheets  Current -Sheets in available spreadsheet.
     * @param string $question_name, $sheet_id
     * @return Google_Service_Drive_DriveFile or Null
     */
    private function check_sheet(array $sheets, string $question_name, $sheet_id){
        foreach($sheets as $sheet){
            if($sheet instanceof Google_Service_Sheets_Sheet){
                if(strcmp($sheet->getProperties()->getTitle(),$question_name) ==0 && strcmp($sheet->getProperties()->sheetId, $sheet_id)!=0 && !is_null($sheet_id))
                    return true;
            }
        }
        return false;
    }
    
    
     /**
     * @name sheet_identifier_helper
     * @author Jesus G. Vega Alejandro
     * @desc Helper method to identify wether a google sheet with a provided name already exists in the google drive.
     * @param array $sheets
     * @param string $questionTitle
     */
    public function sheet_identifier_helper(array $sheets, $questionTitle){
        foreach($sheets as $value){
            if($value instanceof Google_Service_Sheets_Sheet){
                if($value->getProperties()->title == $questionTitle){return true;}
            }
        }
       return false;
    }
    
    
    /**
     * @name manage_heet_create
     * @author Jesus G. Vega Alejandro
     * @desc This method generates the request o create a spreadsheet and updates a reference to it in the local database.
     * @param int $quizID
     * @param string $questionTitle
     */
    public function manage_sheet_create($quizID, $questionTitle, $quizName): ?array {
        //Max API Calls: 2
        $quizTitle = $quizName;
        
        $spreadsheet= $this->service->create_question_sheet($quizTitle, $questionTitle);
        $spreadsheet_id = $spreadsheet[0];
        $sheet_id = $spreadsheet[1];
        //$sheet_id = $this->service->check_sheet($spreadsheet_id)->getSheets()[0]->getProperties()->sheetId;
        
        //Generates a valid redirect link and stores it in the Database
        $sheet_url= 'docs.google.com/spreadsheets/d/' . $spreadsheet_id;
        insert_link($spreadsheet_id,$sheet_url, $quizID, $sheet_id, false);
           
        return [$spreadsheet_id, $sheet_id];
    }
    
   
    /**
     * @name manage_sheet_create
     * @author Jesus G. Vega Alejandro
     * @desc This method generates the request o create a spreadsheet and updates a reference to it in the local database.
     * @param int $quizID
     * @param string $questionTitle
     */
    public function manage_override_sheet_values($spreadsheetID, $sheet_id, $questionTitle, $attempts, $data_range = null, 
        bool $image_included = null, $image_range = null, $studentid, bool $sheet_provided = null, bool $is_external= null, $saved_external = null,
        $student_name, $quizID){
        
        global $DB, $USER;
        
        $student_name = $student_name;
        $qid = $quizID;
        
        /********************************************************WRITING PROCEDURE*************************************************************************/
        
        //Array of datasetdefinitions pertaining to an individual question
        $spreadsheet_writting_values = $_SESSION['spreadsheet_writting_values'];
        $data_variables = $spreadsheet_writting_values['spreadsheet_data_variables'] ?? array();
        $data_values = $spreadsheet_writting_values['spreadsheet_data_values'] ?? array();
        $unit_values = $spreadsheet_writting_values['spreadsheet_unit_values'] ?? array();
        
        
        if(is_null($unit_values) || empty($unit_values)){
            array_unshift($unit_values, "N/A");
        }
        
        //Adding Labels to Array Heads
        array_unshift($data_values,"Assigned Values" );
        array_unshift($data_variables, "Variables" );
        array_unshift($unit_values, "Internal Moodle Units Available");
        
        //Clearing unnecessary values from the retrieved database data and sorting new values array
        $size_variables = sizeof($data_variables);
        $size_values = sizeof($data_values);
        if($data_variables[$size_values-1] == '.$,') unset($data_values[$size_values-1]); 
        if($data_variables[$size_variables-1] == '_separators' || $data_variables[$size_variables-1] =='_order')unset($data_variables[$size_variables-1]);
        
        foreach($data_variables as &$value){
            if($value != 'Variables') {
                $value = $value . '=';
            }
            $value = (array) str_replace('_var_', '', $value);
        }
        
        foreach($unit_values as &$value){
           $value = (array) $value;
        }
        
        $info_student = array((array) "Student:", (array) $student_name);
        $info_question = array((array) "Points Awarded:");
        
        
        //Generate an associative array with the variables as keys to their possible values
        $data_size=sizeof($data_variables);
        if(!is_null($data_size) && !$data_size<=0){
                for($i=0; $i<$data_size; $i++){ 
                    if(!is_null($data_values)){
                        array_push($data_variables[$i], $data_values[$i]);
                    }
                }
                
                if(!is_null($data_range) && !ctype_space($data_range) && strlen($data_range)>0){
                    $save_range = $this->index_generator($data_size, 0, $data_range); 
                    $range = $questionTitle . '!' . $save_range;
                }else{
                    $save_range = $this->index_generator($data_size, 1, $data_range);
                    $range = $questionTitle . '!' . $save_range;
                    
                }
                    
                $save_unit_range = $this->set_unit_range($save_range);
                $unit_range = $questionTitle . '!' . $save_unit_range;
                    
                $student_range = $questionTitle . '!' . 'A1:B1';
                $grade_range = $questionTitle . '!' . 'A2:B2';
                
                
                //Delete previous state based on range
                if(!$is_external){
                    try{
                        $previous_range_exist = $DB->get_record('sheet_range_state', ['user_id' => $USER->id, 'quiz_id' => $qid]);
                        //$sheet_state_obj = $DB->get_record('sheet_range_state', ['user_id' => $USER->id, 'quiz_id' => $qid]);
                        if($previous_range_exist != false){
                        if($previous_range_exist->sheet_range != $save_range){
                            //TODO incorporat batch request when figuring out how to write imge.
                            $data_range_state = $questionTitle . '!' . $previous_range_exist->sheet_range;
                            $unit_range_state = $questionTitle . '!' . $previous_range_exist->unit_range;
                            
                            $data_call = new Google_Service_Sheets_ClearValuesRequest([ 'range' => $data_range_state]);
                            $unit_call = new Google_Service_Sheets_ClearValuesRequest([ 'range' => $unit_range_state]);
                            $data_clear = [$data_range_state ,  $unit_range_state];
                            
                            $body = new Google_Service_Sheets_BatchClearValuesRequest([
                                'ranges' => $data_clear
                            ]);
                            
                            try{
                                $this->values->batchClear($spreadsheetID, $body);
                            }catch(\Exception $e){}
                        }
                        }
                    }catch (dml_missing_record_exception $e) {
                        $previous_range_exist = false;
                    } 
                } else $previous_range_exist = false;
                    
                $data_call = new Google_Service_Sheets_ValueRange([
                        'range' => $range, 
                        'majorDimension' => 'ROWS', 
                        'values'=> $data_variables
                ]);
                    
                $name_call = new Google_Service_Sheets_ValueRange([ 
                        'range' => $student_range, 
                        'majorDimension' => 'COLUMNS', 
                        'values'=> $info_student
                ]);
                    
                $grade_call =new Google_Service_Sheets_ValueRange([ 
                        'range' => $grade_range, 
                        'majorDimension' => 'COLUMNS', 
                        'values'=> $info_question
                    ]);
                
                $unit_call =new Google_Service_Sheets_ValueRange([
                    'range' => $unit_range,
                    'majorDimension' => 'ROWS',
                    'values'=> $unit_values
                ]);
                    
                if(!$is_external && $saved_external==0){
                    $data[] = [$name_call, $data_call, $grade_call, $unit_call];
                }else{
                    $data[] = [$data_call, $unit_call];
                }
                
                $body = new Google_Service_Sheets_BatchUpdateValuesRequest([
                        'valueInputOption' => "RAW",
                        'data' => $data]);
                
                
                try{
                    $this->values->batchUpdate($spreadsheetID, $body);
                }catch(\Exception $e){
                    if($e->getCode() =='400'){
                         $column_call = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
                         'requests' =>['appendDimension' => [
                         'sheetId' => $sheet_id,
                         'dimension' => "COLUMNS",
                         'length' => '4'
                         ]]]);
                        
                         $this->service->batchUpdate($spreadsheetID, $column_call); 
                         $this->values->batchUpdate($spreadsheetID, $body);
                    } 
                }
                    
               //Update the local DB wih references to the sheet 
                if(!is_null($previous_range_exist) && $previous_range_exist != false){
                    $DB->update_record('sheet_range_state', ['id'=>$previous_range_exist->id,
                            'user_id' => $USER->id, 
                            'quiz_id' => $qid, 
                            'sheet_range'=>$save_range,
                            'unit_range' => $save_unit_range
                         ]);
                }else{
                   $DB->insert_record_raw('sheet_range_state', ['user_id' => $USER->id, 
                            'quiz_id' => $qid, 
                            'sheet_range'=>$save_range,
                            'unit_range' => $save_unit_range
                         ]);}
               
                
        }else \core\notification::error("No values to write");

        
    }
    
    
    /**
     * @name index_generator
     * @author Jesus G. Vega Alejandro
     * @desc Generates the appropriate writting range string to use as a call parameter for the Google API.
     * @param array $data, $data_index, int $row_ index 
     * @return string Data Writting Range in Google Sheet API Standard Format (EX. A1:B3)
     */
    private function index_generator($data, int $row_identifier= null, $data_index = null){
        if($row_identifier==1){
            return $this->index_lettert_assignment($data, 0) . '4:C' . strval(3+$data);
        }else{ 
            return $data_index . ':' . $this->index_lettert_assignment($this->letter_converter_helper($data_index[0], 1))
            . strval((int) substr($data_index, 1, strlen($data_index)-1) + $data); //TODO Verify Logic . $column_max
        } 
    }
    
    
    private function index_lettert_assignment($num_size,int $first_toogle=null){
        if($num_size<=0) throw new coding_exception("Invalid array size provided.");
        if(!is_null($first_toogle) && $first_toogle ==0) return 'B';
         if($num_size>0){
            $alphabet = '!ABCDEFGHIJKLMNOPQRSTUVWXYZ';  
            if($num_size<=26){
             return $alphabet[$num_size];
            }else{
                $ret_string = '';
                $num_sets = floor(($num_size/26));
                $num_size = fmod($num_size, 26);
                if($num_size ==0) $num_size=1;
                for($i=1; $i<=$num_sets; $i++){
                    $ret_string = $ret_string . $alphabet[$i];
                }
                return $ret_string . $alphabet[intval($num_size)];
            }
        }
    }
    
    private function letter_converter_helper($str, $offset){
        $alphabet = '!ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if(strlen($str)>1){
            //This soluion is rough; It's functional for the implementation of the plugin due to writting range limitations of a default Google Sheet, but it's hardcoded.
            return (int) 27 + $offset;
        }else return (int) strpos($alphabet, $str) + $offset;
    }
    
    /**
     * @name set_unit_range
     * @author Jesus G. Vega Alejandro
     * @desc Determines the writting range of the unit table in reference to the writting range of the question wildcard data.
     * @param string $range
     * @return string $ret_str - a string containign the complete unit cell range.
     */
    private function set_unit_range(string $range){
        //if(strpos($range ,':') != 'false'){
            $half_str = strpos($range, ':');
            $end_pos = substr($range, $half_str+1, strlen($range));
            $second_char_range = $this->index_lettert_assignment($this->letter_converter_helper($this->purge_numeric_str($end_pos), 2));
            $ret_str = $second_char_range . $this->purge_characters_str(substr($range, 0, $half_str )) . ':' .$second_char_range . $this->purge_characters_str($end_pos);
            return $ret_str;
        //}else throw new coding_exception("Invalid data rage provided while determining unit rage");
    }
    
    
    /**
     * @name purge_characters_str
     * @author Jesus G. Vega Alejandro
     * @desc This method removes all non numerical characters from a given string.
     * @param string $str
     */
    private function purge_characters_str(string $str){
        return implode(str_split(preg_replace("/[^0-9,.]/", "", $str)));
    }
    
    /**
     * @name purge_numeric_str
     * @author Jesus G. Vega Alejandro
     * @desc This method removes all numerical characters from a given string.
     * @param string $str
     */
    private function purge_numeric_str(string $str){
        return implode(str_split(preg_replace("/\d+/", "", $str)));
    }
    
    
    
}





