<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

require_once("$CFG->dirroot/lib/formslib.php");
require_once("$CFG->dirroot/local/spreadsheet/lib.php");
require_once("$CFG->dirroot/local/spreadsheet/style/style.css");



class sheet_form_block_description{
    public  $quizid, $quizname, $cmid, $b_qid, $b_qname, $attemps, $attemptid, $student_Id, $course_Id, $student_name, $page;
    
    
    public function __construct( $quizID, $quizName, $question_id, $question_name, $cmid, $attempts, $attemptid, $student_id, $course_id, $page, $student_name){
        
        $this->quizid     = $quizID;
        $this->quizname   = $quizName;
        $this->b_qid      = $question_id;
        $this->b_qname    = $question_name;
        $this->cmid       = $cmid;
        $this->attemps    = $attempts;
        $this->attemptid  = $attemptid;
        $this->student_Id = $student_id;
        $this->course_Id  = $course_id;
        $this->page       = $page;
        $this->student_name = $student_name;
    }
    
}

/**
 * @author Jesus G. Vega Alejandro
 * @desc Simple HTML Form Manager to insert as block in the quiz review page. Extended for the purpose of modifying if needed
 */
class block_form_sheets extends sheet_form_block_description{
  
    public function def(): ?string{
       global $DB, $USER;
       
       //Add Simple redirect button to form if a sheet already exists for the quiz. 
       if($DB->record_exists('spreadsheets_validation', ['user_id'=>$USER->id, 'quiz_id'=>$this->quizid])){
           $redirect_url = get_link($this->quizid);
           $hidden_options = "<a href= 'https://$redirect_url' target='_blank'>
           <input type ='button'  name='Available_Sheet' value= 'Open Spreadsheet'></a>";
       }else{
           $hidden_options = '';
       }
       $help_string = get_string("HELP_BUTTON_TIPS", "local_spreadsheet");
       $form_content = "<form method='post'>
          <label for='Generation'>
            <h5>Generate Google Spreadsheet</h5> 
          </label>
          <br><br>
          <label for='SheetRange'>
            <font color = #0f6fc5>Sheet Range:</font>
          </label>
          <input type='text' name='SheetRange' placeholder='Starting Cell Ex. A4'>
            <div class='sheets-help'>
                 <p>$help_string</p>
            </div><br><br>
          <label for='SheetLink'>
            <font color = #0f6fc5> Sheet Link:</font> 
            <font color = #FFFFFF>' '
            </font>
          </label>
          <input type='text' name='SheetLink' placeholder='Available Sheet link if any.'><br><br>
          <a><input type='submit' class='button' name='Sheet' value= 'Generate Spreadsheet'></a>
          </form>";
            
            if(array_key_exists('Sheet', $_POST)){ 
                $sheet_data_range =$_POST['SheetRange'];
                $image_range = null;
                $sheet_link = $_POST['SheetLink'];
            
                $this->execute_redirect($sheet_data_range, $image_range, $sheet_link);
            }
            
        return $form_content . $hidden_options;
    }
    
    
    /**
     * @name execute_redirect
     * @author Jesus G. Vega Alejandro
     * @desc Manage Question values to be handled through current session and redirects to authentication page.
     * @param string data_range, $sheet_link
     */
    
    private function execute_redirect(string $data_ranges = null, string $image_range = null, $sheet_link = null){
          $validate = true;
          if(!$this->evaluate_ranges($data_ranges, $image_range)){
              $evaluation = $this->evaluate_data_range($data_ranges, $sheet_link);
              if($evaluation == 'second scenario'){
                  \core\notification::error("The range provided is invalid. On columns 'A' and 'B', range A1:B1 to A1:B3 are reserved");
                  $validate = !$validate;
              }elseif($evaluation == 'first scenario'){
                  \core\notification::error("The range provided is invalid. Range given must be of up to 2 characters. Moodle will only write to A-Z locations on Google Spreadsheets. Ex A1 is acceptable. AA1 is not a valid range");
                  $validate = !$validate;
              }elseif($evaluation == 'third scenario'){
                  \core\notification::error("The range provided is invalid. Incorrect format used for range.");
                  $validate = !$validate;
              }elseif($evaluation == 'fourth scenario'){
                  \core\notification::error("The range provided is invalid. Writting range number must not be 0.");
                  $validate = !$validate;
              }elseif($evaluation == 'fifth scenario'){
                  \core\notification::error("The range provided is invalid. Characters must be upper case. Ex. A4 ");
                  $validate =!$validate;
              }
              
              //If invalid user input, redirect to the review attempt page.
              if(!$validate) redirect(new moodle_url("/mod/quiz/review.php?attempt=" . $this->attemptid. "&cmid=" .
                  $this->cmid . '&showall=0'. '&page='. $this->page));
             
          }
          
          $sheet_format = $this->link_format($sheet_link);
          $sheetid = $sheet_format[1];
          if($sheetid != 'false'){
              
          //Clear table protocol. Check complexity in large DB
          $this->clear_table_values($this->course_Id);
            
          $_SESSION['spreadsheets_values'] = [
                'spreadsheet_question_id'=>$this->b_qid,
                'spreadsheet_question_name'=>$this->b_qname,
                'spreadsheet_quizId'=>$this->quizid,
                'spreadsheet_student_id'=>$this->student_Id,
                'spreadsheet_data_range'=>$data_ranges,
                'spreadsheet_image_range'=>$image_range,
                'spreadsheet_gs_id'=>$sheetid,
                'spreadsheet_cmid'=> $this->cmid,
                'spreadsheet_page'=> $this->page,
                'spreadsheet_attempts'=>$this->attemps,
                'spreadsheet_attemptid'=> $this->attemptid,
                'spreadsheet_student_name'=> $this->student_name,
                'spreadsheet_quiz_name'=> $this->quizname,
                "spreadsheet_sheet_id" => $sheet_format[0]
          ];
            
          redirect(new moodle_url("/local/spreadsheet/login.php"));
            
          }
      
    }
    
   
   /**
    * @name link_format
    * @author Jesus G. Vega Alejandro
    * @desc Manage provided sheet link to identify SpreadsheetId and sheetId
    * @param string* $sheet_link
    */
    private function link_format(string &$sheet_link): ?array {
        $beggin_pos = strpos($sheet_link, '/d/' );
        $end_pos = strpos($sheet_link, '/edit');
        $sheet_id_pos = strpos($sheet_link, 'gid=');
        $spreadsheet_sheet_id = null;
        $string_validation = null;
        
        if(is_int($sheet_id_pos)){
            $spreadsheet_sheet_id = substr($sheet_link, $sheet_id_pos + 4);
        }
        
        //TODO: Remember to verify these lengts for substr
        if(is_int($beggin_pos) && is_int($end_pos)){
            $string_validation = substr($sheet_link, $beggin_pos + 3, $end_pos - $beggin_pos -3);
        }elseif(is_int($beggin_pos) && is_null($end_pos)) {
            $string_validation = substr($sheet_link, $beggin_pos + 3);
        }elseif($sheet_link== '' || ctype_space($sheet_link) || strlen($sheet_link) ==0){
            $string_validation = null;
        }else{
            if(!is_null($sheet_link)) \core\notification::error(get_string('Invalid_Sheet_Link', 'local_spreadsheet'));
            $string_validation = 'false';
        }
        return [$spreadsheet_sheet_id, $string_validation];
    }
    
    
    private function evaluate_ranges(string $data_ranges = null, string $image_range = null){
        //Eliminate two main error input possibilities
        return ($data_ranges==null && $image_range==null) || ($data_ranges=='' && $image_range=='');
    }
    
    private function character_limit_data_range($data_ranges){
        $count = 0;
        $chars = str_split($data_ranges);
        foreach($chars as $value){
            if(!is_numeric($value)) $count++;
        }
        return $count;
    }
    
    private function evaluate_data_range(string $data_ranges, string $sheet_link = null): ?string{
        if($this->character_limit_data_range($data_ranges)>1){
            return 'first scenario';
        } elseif(strcasecmp($data_ranges[1], "0") == 0){
            return 'fourth scenario';
        }elseif(!ctype_upper($data_ranges[0])){
            return 'fifth scenario';
        }elseif((strlen($data_ranges) <=2 && (strcasecmp($data_ranges[0], 'A') == 0 || strcasecmp($data_ranges[0], 'B') == 0)) && (is_null($sheet_link) || empty($sheet_link))){
            if($data_ranges[1] == '1' || $data_ranges[1] == '2' || $data_ranges[1] == '3'){
                return 'second scenario';
            }
        }elseif(!is_string($data_ranges[0]) && !is_int((int) $data_ranges[1])){
            return 'third scenario';
       
        } 
        return null;
    }
    
    /**
     * @name clear_table_values
     * @author Jesus G. Vega Alejandro
     * @desc Verify and remove non required table values from 'spreadsheet_validation' and 'sheet_range_state'
     * @param int $courseID
     */
    private function clear_table_values($courseID){
        global $DB, $USER;
        
        //Array of all quiz ID sharng the same user id that have sheets created.
        //TODO Verify Excepton Handle if 'fieldset_select' does not find any compaitble records.
        $available_sheets = $DB->get_fieldset_select('spreadsheets_validation', 'quiz_id', "user_id=$USER->id");
        $available_quiz = $DB->get_fieldset_select('quiz', 'id', "course=$courseID");
        
        $comp_array = array_diff($available_sheets, $available_quiz);
        
        if(!is_null($comp_array) && !empty($comp_array)){
            foreach($comp_array as $value){
                if(in_array($value, $available_sheets)){
                    remove_link($value, $USER->id);
                }
            }
        }
    }
   
    
}