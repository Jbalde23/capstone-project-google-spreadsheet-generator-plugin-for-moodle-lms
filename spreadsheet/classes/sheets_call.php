<?php
use Google\Exception;
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


defined('MOODLE_INTERNAL') || die();


/**
 * @name google_spreadsheet_resources
 * @author Jesus G. Vega Alejandro
 * @desc This class contains a variety of custom functions which facilitate custom POST and UPDATE request to Google Services
 */

class Google_Spreadsheet_Resources extends Google_Service_Sheets_Resource_Spreadsheets{
    
    /**
     * @author Jesus G. Vega Alejandro
     * @desc This method modifies an existing worksheet to add a new sheet.
     * @return string worksheetID - The the ID of the modified worksheet as confirmation.
     */
    public function add_sheet_to_worksheet($spreadsheetId, $sheetTitle){
        //Number of queries perfomed in funtion: 1 tentatively
        
        //Generate both the sheet generation request and the batch update request in order to alter an existing worksheet
        $sheet_request = new Google_Service_Sheets_Request(['addSheet' => ['properties' =>['title' =>$sheetTitle]]]);
        $batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest(['requests' => $sheet_request]);
        
        //Batch Update Request
        $batch_response = $this->batchUpdate($spreadsheetId, $batchUpdateRequest);
        
        return $batch_response->getSpreadsheetId();
    }
    
    /**
     * @author Jesus G. Vega Alejandro
     * @desc This method will update the tab name depending on the question .
     * @param  $spreadsheetTitle, $sheetTitle
     * @return string ID of the newly generated spreadsheet
     */
    public function update_question_sheet_title($spreadsheetId, $sheetTitle, $sheet_id = null){
        //Total: 1 API Call
        $spreadSheet = $this->get($spreadsheetId);
        $sheet_request = new Google_Service_Sheets_Request(['updateSheetProperties' => ['properties' =>
            ['title' => $sheetTitle, 'sheetId' => $sheet_id],
            'fields' => 'title']]);
        $batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest(['requests' => $sheet_request]);
        $this->batchUpdate( $spreadSheet->spreadsheetId, $batchUpdateRequest);
    }
    
    /**
     * @author Jesus G. Vega Alejandro
     * @desc This method will generate a Google Drive Sheet with the question title set.
     * @param  $spreadsheetTitle, $sheetTitle
     * @return string ID of the newly generated spreadsheet
     */
    public function create_question_sheet($spreadsheetTitle, $sheetTitle){
        //Number of queries performed in function: 1
        
        $spreadsheet = new Google_Service_Sheets_Spreadsheet(['properties' =>
            ['title' => $spreadsheetTitle],
            'sheets' =>['0'=>['properties' =>['title'=> $sheetTitle]]]]);
       $spreadsheet = $this->create($spreadsheet);
       $sheetID = $spreadsheet->getSheets()[0]->getProperties()->sheetId;
       return [$spreadsheet->spreadsheetId, $sheetID];
    }
    
    /**
     * @author Jesus G. Vega Alejandro
     * @desc Event Handler for 404: Not Found Exception while validatng the existance of a worksheet in the Google Drive.
     * @param  $spreadsheetId
     * @return mixed Requested object of type Google_Service_Sheets_Spreadsheet or null
     */
    public function check_sheet($spreadsheetId): ?Google_Service_Sheets_Spreadsheet{
        try{ $sheet = $this->get($spreadsheetId);
        }catch(\Exception $e){ if($e->getCode()=='404') $sheet = null;}
        return $sheet;
    }
    
    
}




