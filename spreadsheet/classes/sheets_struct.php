<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


defined('MOODLE_INTERNAL') || die();


/**
 * @name Google_Spreadsheet
 * @desc Extended Class of the standard service definition for Sheets (v4) to integrate our custom classes.
 */
class Google_Spreadsheet extends Google_Service_Sheets{
    
    //Override to manage custom methods
    public function __construct(Google_Client $client, $rootUrl = null)
    {
        parent::__construct($client);
        $this->rootUrl = $rootUrl ?: 'https://sheets.googleapis.com/';
        $this->servicePath = '';
        $this->batchPath = 'batch';
        $this->version = 'v4';
        $this->serviceName = 'sheets';
        
        $this->spreadsheets = new Google_Spreadsheet_Resources(
            $this,
            $this->serviceName,
            'spreadsheets',
            array(
                'methods' => array(
                    'batchUpdate' => array(
                        'path' => 'v4/spreadsheets/{spreadsheetId}:batchUpdate',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'spreadsheetId' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'create' => array(
                        'path' => 'v4/spreadsheets',
                        'httpMethod' => 'POST',
                        'parameters' => array(),
                    ),'get' => array(
                        'path' => 'v4/spreadsheets/{spreadsheetId}',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'spreadsheetId' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'includeGridData' => array(
                                'location' => 'query',
                                'type' => 'boolean',
                            ),
                            'ranges' => array(
                                'location' => 'query',
                                'type' => 'string',
                                'repeated' => true,
                            ),
                        ),
                    ),'getByDataFilter' => array(
                        'path' => 'v4/spreadsheets/{spreadsheetId}:getByDataFilter',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'spreadsheetId' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ), 'add_sheet_to_worksheet' => array(
                        'path' => 'v4/spreadsheets',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'spreadsheetId' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'questionTitle' => array(
                                'type' => 'string',
                                'required' => true,
                            )
                        )
                        
                    ), 'create_question_sheet' => array(
                        'path' => 'v4/spreadsheets',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'questionTitle' => array(
                                'type' => 'string',
                                'required' => true,
                            )
                        ), 'quizTitle' => array(
                            'type' => 'string',
                            'required' => true,
                        )
                    )
                )
            )
            );
        $this->spreadsheets_developerMetadata = new Google_Service_Sheets_Resource_SpreadsheetsDeveloperMetadata(
            $this,
            $this->serviceName,
            'developerMetadata',
            array(
                'methods' => array(
                    'get' => array(
                        'path' => 'v4/spreadsheets/{spreadsheetId}/developerMetadata/{metadataId}',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'spreadsheetId' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'metadataId' => array(
                                'location' => 'path',
                                'type' => 'integer',
                                'required' => true,
                            ),
                        ),
                    ),'search' => array(
                        'path' => 'v4/spreadsheets/{spreadsheetId}/developerMetadata:search',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'spreadsheetId' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),
                )
            )
            );
        $this->spreadsheets_sheets = new Google_Service_Sheets_Resource_SpreadsheetsSheets(
            $this,
            $this->serviceName,
            'sheets',
            array(
                'methods' => array(
                    'copyTo' => array(
                        'path' => 'v4/spreadsheets/{spreadsheetId}/sheets/{sheetId}:copyTo',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'spreadsheetId' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'sheetId' => array(
                                'location' => 'path',
                                'type' => 'integer',
                                'required' => true,
                            ),
                        ),
                    ),
                )
            )
            );
        $this->spreadsheets_values = new Google_Service_Sheets_Resource_SpreadsheetsValues(
            $this,
            $this->serviceName,
            'values',
            array(
                'methods' => array(
                    'append' => array(
                        'path' => 'v4/spreadsheets/{spreadsheetId}/values/{range}:append',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'spreadsheetId' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'range' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'includeValuesInResponse' => array(
                                'location' => 'query',
                                'type' => 'boolean',
                            ),
                            'insertDataOption' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'responseDateTimeRenderOption' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'responseValueRenderOption' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'valueInputOption' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                        ),
                    ),'batchClear' => array(
                        'path' => 'v4/spreadsheets/{spreadsheetId}/values:batchClear',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'spreadsheetId' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'batchClearByDataFilter' => array(
                        'path' => 'v4/spreadsheets/{spreadsheetId}/values:batchClearByDataFilter',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'spreadsheetId' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'batchGet' => array(
                        'path' => 'v4/spreadsheets/{spreadsheetId}/values:batchGet',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'spreadsheetId' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'dateTimeRenderOption' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'majorDimension' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'ranges' => array(
                                'location' => 'query',
                                'type' => 'string',
                                'repeated' => true,
                            ),
                            'valueRenderOption' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                        ),
                    ),'batchGetByDataFilter' => array(
                        'path' => 'v4/spreadsheets/{spreadsheetId}/values:batchGetByDataFilter',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'spreadsheetId' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'batchUpdate' => array(
                        'path' => 'v4/spreadsheets/{spreadsheetId}/values:batchUpdate',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'spreadsheetId' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'batchUpdateByDataFilter' => array(
                        'path' => 'v4/spreadsheets/{spreadsheetId}/values:batchUpdateByDataFilter',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'spreadsheetId' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'clear' => array(
                        'path' => 'v4/spreadsheets/{spreadsheetId}/values/{range}:clear',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'spreadsheetId' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'range' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'get' => array(
                        'path' => 'v4/spreadsheets/{spreadsheetId}/values/{range}',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'spreadsheetId' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'range' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'dateTimeRenderOption' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'majorDimension' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'valueRenderOption' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                        ),
                    ),'update' => array(
                        'path' => 'v4/spreadsheets/{spreadsheetId}/values/{range}',
                        'httpMethod' => 'PUT',
                        'parameters' => array(
                            'spreadsheetId' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'range' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'includeValuesInResponse' => array(
                                'location' => 'query',
                                'type' => 'boolean',
                            ),
                            'responseDateTimeRenderOption' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'responseValueRenderOption' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'valueInputOption' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                        ),
                    ),
                )
            )
            );
    }
}