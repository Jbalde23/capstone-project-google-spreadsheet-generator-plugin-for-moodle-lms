<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


defined('MOODLE_INTERNAL') || die();


require_once("$CFG->dirroot/local/spreadsheet/api/google/apiclient-services/src/Google/Service/Sheets.php");
require_once("$CFG->dirroot/lib/google/src/Google/Client.php");




class Google_Client_mod extends Google_Client{
    
    private $simple_API_key = 'AIzaSyDV_CZm5BilA6VlSwfdRTenC1w-162l79A';
    /**
     * Build a google ready OAuth 2 service.
     * @param JSON Authentication File provided by Google Developers Console
     * @param $name - Application name (Optional Parameter)
     */
    
     public function format_client($json, $scopes, $name=null, $params=null) {
            //JSON Modified to include the API access key 
            $file_data = json_decode($json);
            if(is_null($file_data)) throw new Exception("JSON conversion is null");
            
            //Set Essential Authorization Codes for the Project 
            if(!is_null($name))$this->setApplicationName($name);
            
            $this->setClientId($file_data->client_id);            
            $this->setClientSecret($file_data->client_secret);
            //$this->setDeveloperKey($file_data->API_Key);
            $this->setScopes($scopes);
            
            if(isset($file_data->redirect_uris)) {
               // $this->setRedirectUri($file_data->redirect_uris[0]);
                if(!is_null($params)){
                    $this->setRedirectUri($file_data->redirect_uris[0] . $params);
                    
                } else $this->setRedirectUri($file_data->redirect_uris[0]);
            }   
     }
     
     public function setConfig($name, $value)
     {
         $this->config[$name] = $value;
     }

}


