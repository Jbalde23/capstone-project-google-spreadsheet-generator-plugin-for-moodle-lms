<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file is meant to have datbase upgrade code that is called from lib/db/upgrade.php, 
 * and also check methods that can be used for pre-install checks via admin/environment.php and lib/environmentlib.php.
 * @package    local_spreadsheet
 * @author     Jesus G. Vega Alejandro
 */


defined('MOODLE_INTERNAL') || die();


/**
 * Insert code for changes to implement any future releases
 */

