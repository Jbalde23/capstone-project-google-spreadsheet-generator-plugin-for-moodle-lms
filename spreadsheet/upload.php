<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


require_once('../../config.php');

global $CFG;

$target_dir = "$CFG->dirroot/local/spreadsheet/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$validation = 0;
$fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$fileName = basename($_FILES["fileToUpload"]["name"]);

// Check the file submited is of JSON Type 
if(isset($_POST["submit"])) {
    if (($fileType == "application/json" || $fileType = 'json') && $fileName== "credentials.json") {
        $validation = 1;
    }else{
        \core\notification::error("The file provided was either not a JSON File or the name was incorrect. Name must be 'credentials.json'");
        redirect(new moodle_url("/local/spreadsheet/configure.php"));
    }
}

if($validation==1){
    if (file_exists($target_file)) {
        //Remove file from directory with the intention of replacing it 
        unlink($target_file);
        $validation = 1;
    } 
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        \core\notification::info("The file has been uploaded correctly");
        redirect(new moodle_url("/local/spreadsheet/configure.php"));
    }else{
        \core\notification::error("There was an error uploading your file. Please try again.");
        redirect(new moodle_url("/local/spreadsheet/configure.php"));
    } 
}

?>