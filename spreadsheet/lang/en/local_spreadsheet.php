<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component spreadhsheet, language 'en', branch 'MOODLE_3_10_STABLE'
 *
 * @package    local
 * @subpackage spreadsheet
 * @copyright  2021 Jesus G. Vega Alejandro
 */


defined('MOODLE_INTERNAL') || die();

global $CFG;

$string['SUCCESS_Google_authentication'] = 'Successfully authenticated user fot Google Platform';
$string['FAILED_Google_authentication'] = "Failed authentication, verify credentials.";

//Configuration Error
$string['MISSING_JSON_TEACHER'] = 'Configuration file missing from the plugin; Please contact your institutions support team for further details.';
$string['MISSING_JSON_ADMIN'] = "The file 'credentials.json' seems to be missing from the project. Please visit console.developers.google.com in order to obtain a new credentials file.
IMPORTANT: If the provided JSON contains a key (web or local) please remove said key before uploading; file name must be uploaded with the name 'credentials.json'";


//CSS Frontend Help Tips
$string['HELP_BUTTON_TIPS'] = 'This option will generate a grading spreadsheet with reference to moodle, with the values pertaining to the current user and question being evaluated.
<br><br>The Sheet Range will detail the starting cell in which the question data will be written in the sheet.The Sheet Link will write the question information to a provided spreadsheet and will link it to Moodle. Moodle will keep updating spreadsheets for grading.
A sheet with default writting range will be generated in case these are not filled.
<br><br>The Open Spreadsheet button will open a tab to the spreadsheet belonging to this question if vailable. <br><br>
These features are only available to "calculated" type questions and subtypes (multiform and simple).';

//Messages in relation to Spreadsheet Form
$string['Generate_Spreadsheet'] = 'Spreadsheet Generation';
$string['Generate_Spreadsheet_help'] = 'This feaure will allow educators to generate a Google Spreadsheet with the available data for the wildcards';

//Messages in Relation to Upload Form
$string['UPLOAD_FORM'] = "<h5>For Site Administrators</h5><br>   Please use the following features to upload a valid Google Credentials file onto Moodle<br>
To obtain a valid credentials file, instructions are as followed: <br>
    <p style='margin-left:10%; margin-right:10%;'>1) Go to console.developers.google.com and access your project site. 
    If you currently don't have a project set up, please follow the site instructions to do so; 
    Project must have both Google Drive and Google Sheet API capabilities active.<br>
    2) On the Credentials Page on the Google Console, create a credentials document if one is not created and download it.<br>
    3) <b>The file will contain a type key in the inner text. 'web' is a prominent type key utilized. Erase the initial type key and the first set of keys ({}).<br>
    </b> 4) <b>Before uploading, rename the file 'credentials.json' .</b></p>   ";

$string['UPLOAD_FORM_PERMISSION_DENIED'] = 'This user does not posses the necessary permissions to access the features in this page; Please contact an administrator at your institution';

//Messages in relation to Exceptions 
$string['Invalid_Client'] = 'Invalid Client Credentials. Access Tokens non valid';
$string['Invalid_Sheet_Link'] = "Invalid Sheet Link Provided.";
$string['Sheet_Not_Found'] = "Invalid Spreadsheet Link provided. The sheet was not found.";
$string['Duplicate_Shet_Title'] = "There is already an existing Sheet with the question name within the provided spreadshet. Please change the name or delete the sheet.";
$string['REQUEST_ERROR'] = "Something went wrong. Please try again later";

//Request API Access
$string['GOOGLE_API_MANAGE_DRIVE'] = 'https://www.googleapis.com/auth/drive';
$string['GOOGLE_API_MANAGE_SHEETS'] = 'https://www.googleapis.com/auth/spreadsheets';

//Relevant URLs 
$string['GOOGLE_DOCS_SPREADSHEETS'] = 'docs.google.com/spreadsheets/d/';
$string['MOODLE_QUIZ_EDITING_PAGE_DEFAULT'] = "$CFG->dirroot/mod/quiz/edit.php?";

