// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Readme file for local customisations
 *
 * @package    local_Sreadsheet
 */

API directory
==============================
This directory contains Google Recommended API to configure connectivity, authentication, nd request management to the Google Console.
Code Structure Developed by Google In.


Developers Commentary
==============================
Included in the plugin is a renewed version of the Google API tools compared to those present in moodle/lib/google.

Moodle included Tools posses deprecated features by Google Current Standards

API Code
==============================
The files included in /spreadsheets/api are not proprietry code of main plugin author or of moodle in any kind. Use of said file permitted by community and 
each individual author/company is credited in each file. 