<?php

defined('MOODLE_INTERNAL') || die();
global $CFG;

$base_dir = '/local/spreadsheet/';

//Necessary Files for Client Authentication Procedures
require_once("$CFG->dirroot/lib/google/src/Google/Service.php");
require_once("$CFG->dirroot/lib/google/src/Google/Service/Resource.php");
require_once("$CFG->dirroot/lib/google/src/Google/Service/Drive.php");

//Necessary Files for Sheet Generation and Update Procedures
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/Spreadsheet.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/Response.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/Sheet.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/Request.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/ValueRange.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/SheetProperties.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/AppendValuesResponse.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/ClearValuesRequest.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/ClearValuesResponse.php");

require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/BatchClearValuesRequest.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/BatchClearValuesResponse.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/BatchUpdateSpreadsheetRequest.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/BatchUpdateValuesRequest.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/BatchUpdateValuesResponse.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/BatchUpdateSpreadsheetResponse.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/Resource/SpreadsheetsValues.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/Resource/Spreadsheets.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/Resource/SpreadsheetsDeveloperMetadata.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/Resource/SpreadsheetsValues.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/Resource/SpreadsheetsSheets.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/UpdateBordersRequest.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/UpdateSpreadsheetPropertiesRequest.php");
require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/UpdateDimensionPropertiesRequest.php");
//require_once("$CFG->dirroot$base_dir"."api/google/apiclient-services/src/Google/Service/Sheets/AppendDimensionResponse.php");

require_once("$CFG->dirroot$base_dir" ."api/google/apiclient-services/src/Google/Service/Sheets/Resource/SpreadsheetsSheets.php");
require_once("$CFG->dirroot$base_dir" ."classes/service.php");
require_once("$CFG->dirroot$base_dir"."classes/data_form_mod.php");


require_once("$CFG->dirroot$base_dir"."classes/sheets_call.php");
require_once("$CFG->dirroot$base_dir"."classes/sheets_struct.php");
require_once("$CFG->dirroot$base_dir"."classes/sheets_service.php");

