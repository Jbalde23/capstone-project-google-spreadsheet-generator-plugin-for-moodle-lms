<?php


defined('MOODLE_INTERNAL') || die();



/**
 *
 * Library of funcion related to Spreadsheet Capabilities.
 *
 * @package    local_spreadsheet
 * @subpackage lib
 * @author     Jesus G. Vega Alejandro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */





function local_spreadsheet_extend_navigation_course(navigation_node $navigation, $course, $context){
    global $COURSE, $USER;
    $scontext = context_course::instance($COURSE->id);
    if(has_capability('moodle/site:config', $scontext) && is_siteadmin($USER->id)){
        //$navigation->fin
        $spreadsheetnode = $navigation->add('Upload Google Workspace Credentials', new moodle_url('/local/spreadsheet/configure.php'),
            navigation_node::NODETYPE_LEAF, null, null, new pix_icon( 'i/import', ''));
    }
}

/**
 * @name get_link
 * @author Jesus G. Vega Alejandro
 * @desc Rendering link in the quiz editing page.
 * @param int $quizID
 */

function get_link($quizID){
    global $DB, $CFG, $USER;
    require_once("$CFG->dirroot/local/spreadsheet/classes/data_form_mod.php");
    $redirect_url = '';
    
    if($DB->record_exists('spreadsheets_validation', ['user_id'=> $USER->id, 'quiz_id' => $quizID, 'is_created' => 1])){
        $redirect_url = ($DB->get_record('spreadsheets_validation', ['user_id'=> $USER->id, 'quiz_id' => $quizID, 'is_created' => 1]))->sheet_link;
       
    }
    
    return $redirect_url;
}

/**
 * @name insert_link
 * @author Jesus G. Vega Alejandro
 * @desc Generates and inserts a valid redirect link address into the designated databas etable (spreadsheets_validation)
 * @param String $store_url
 * @param int $quizId 
 * 
 */
function insert_link($spreadsheet_id, $store_url, $quizId, $single_sheet_id, bool $is_external=null){
    global $DB, $USER;
    
    //Question link already exists in the DB
   // if(!$DB->record_exists('spreadsheets_validation', ['user_id'=> $USER->id, 'quiz_id' => $quizId, 'is_created' => 1])){
        
        $update_record = new stdClass();
        $update_record->quiz_id = $quizId;
        $update_record->user_id = $USER->id;
        $update_record->is_created = 1;
        $update_record->single_sheet_id = $single_sheet_id;
        $update_record->sheet_id = $spreadsheet_id;
        $update_record->sheet_link = $store_url;
        
        if($is_external){
            $update_record->is_external = 1;
        }else $update_record->is_external = 0;
        
        $DB->insert_record_raw('spreadsheets_validation', $update_record);
        
    //}else{
   /*     $record_id = $DB->get_field('spreadsheets_validation', 'id' , ['user_id'=> $USER->id, 'quiz_id' => $quizId]);
        
        $update_record = new stdClass();
        $update_record->id = $record_id;
        $update_record->user_id = $USER->id;
        $update_record->quiz_id = $quizId;
        $update_record->is_created = 1;
        $update_record->single_sheet_id = $single_sheet_id;
        $update_record->sheet_id = $spreadsheet_id;
        $update_record->sheet_link = $store_url;
        
        if($is_external){
            $update_record->is_external = 1;
        }else $update_record->is_external = 0;
        
        $DB->update_record('spreadsheets_validation', $update_record);
    }*/
}


/**
 * @name remove_link
 * @author Jesus G. Vega Alejandro
 * @desc Removes a spreadsheet redirect link from the local database (spreadsheets-validation)
 * @param String $sheetsID
 * @param int $quizId
 *
 */
function remove_link($quiz_id, $user_id, $ignore_sv = null){
    global $DB;
    if(!is_null($ignore_sv) && is_int($ignore_sv) && $ignore_sv == 1){
        $DB->delete_records('sheet_range_state' , ['user_id'=> $user_id, 'quiz_id' => $quiz_id]);
    }else{
        $DB->delete_records('spreadsheets_validation' , ['user_id'=> $user_id, 'quiz_id' => $quiz_id, 'is_created' => 1]);
        $DB->delete_records('sheet_range_state' , ['user_id'=> $user_id, 'quiz_id' => $quiz_id]);
    }
    
}


/**
 * @name replaceContentInFile
 * @desc Inserts Content Onto File
 * @param String $sheetsID
 * @param int $quizId
 *
 */
function replaceContentInFile($original, $toReplace, $file){
    $buf = "";
    $files = file($file);
    foreach($files as $line){
        $buf .= preg_replace("|".$original."[A-Za-z_.]*|", $original.$toReplace, $line);
    }
    fclose($files);
    //echo $buf;
    file_put_contents($file, $buf);
}


