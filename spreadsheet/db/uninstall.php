<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Uninstall File
 *
 * @package    local_spreadsheet
 * @copyright  2021 Jesus G. Vega Alejandro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @var stdClass $plugin
 */

//Runs code to substitute files stored in Backup with altered files 

defined('MOODLE_INTERNAL') || die();

function xmldb_local_spreadsheet_uninstall(){
    global $CFG;
    
    copy("$CFG->dirroot/local/spreadsheet/installation/original/review.php", "$CFG->dirroot/mod/quiz/review.php");  
    
}

